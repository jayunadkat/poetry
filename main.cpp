#include <iostream>
#include <random>

#include "load_data.hpp"
#include "types.hpp"


template <typename T>
void filter(std::vector<T>& V, unsigned n)
{
    static std::random_device device;
    static std::mt19937 generator(device());
    std::vector<T> result;
    for (unsigned i = 0; i < n; ++i)
    {
        std::uniform_int_distribution<int> distribution(0, V.size() - 1);
        int index = distribution(generator);
        result.push_back(V[index]);
        V.erase(V.begin() + index);
    }
    V = result;
}

std::pair<bool, Match> check_word(const Word& word, const Match& previous)
{
    if (previous.pattern.size() < word.syllables.size())
        return {false, {{}, {}}};

    for (std::size_t i(0); i < word.syllables.size(); ++i)
        if (!word.syllables[i].match(previous.pattern[i]))
            return {false, {{}, {}}};

    Match result = previous;
    result.pattern.erase(result.pattern.begin(),
            result.pattern.begin() + word.syllables.size());
    result.sentence.push_back(word);
    return {true, result};
}

int main(int argc, char** argv)
{
    std::vector<Word> dictionary = read_dictionary(read_file("cmudict-0.7b"));
    std::vector<Syllable> pattern {
        UnstressedSyllable, StressedSyllable,
        UnstressedSyllable, StressedSyllable,
        UnstressedSyllable, Syllable {"EY", true}};

    filter(dictionary, 500);
    std::cout << "==== dictionary ====" << std::endl;
    for (auto&& w : dictionary)
    {
        std::cout << ' ' << w.value;
        for (auto&& s : w.syllables)
            std::cout << ' ' << s.code << s.stressed;
        std::cout << std::endl;
    }
    std::cout << std::endl << std::endl;

    std::vector<Match> matches {{{}, pattern}}, new_matches;
    bool done = false;
    unsigned counter = 0;
    while (!done)
    {
        done = true;
        new_matches.clear();
        for (auto&& match : matches)
        {
            if (match.pattern.size() == 0)
            {
                new_matches.push_back(match);
                continue;
            }
            for (auto&& word : dictionary)
            {
                auto result = check_word(word, match);
                if (result.first)
                {
                    new_matches.push_back(result.second);
                    if (result.second.pattern.size() > 0)
                        done = false;
                }
            }
        }
        matches = new_matches;

//        std::cout << "==== " << ++counter << " word(s) ====" << std::endl;
//        for (auto&& m : matches)
//        {
//            for (auto&& w : m.sentence)
//                std::cout << ' ' << w.value;
//            std::cout << "// ";
//            for (auto&& w : m.sentence)
//                for (auto&& s : w.syllables)
//                    std::cout << ' ' << s.code << s.stressed;
//            std::cout << "// ";
//            for (auto&& s : m.pattern)
//                std::cout << ' ' << s.code;
//            std::cout << std::endl;
//        }
//        std::cout << std::endl << std::endl;
    }

        std::cout << "==== matches ====" << std::endl;
        for (auto&& m : matches)
        {
            for (auto&& w : m.sentence)
                std::cout << ' ' << w.value;
            std::cout << " //";
            for (auto&& w : m.sentence)
                for (auto&& s : w.syllables)
                    std::cout << ' ' << s.code << s.stressed;
            std::cout << std::endl;
        }

        std::cout << std::endl << matches.size() << " matches!" << std::endl;

    return 0;
}

