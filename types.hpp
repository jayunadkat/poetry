#ifndef TYPES_HPP
#define TYPES_HPP

#include <string>
#include <vector>

struct Syllable
{
    std::string code = "";
    bool stressed = false;

    bool match_code = false;
    bool match_stress = false;

    Syllable(const std::string& code = "", const bool& stressed = false,
            const bool& match_code = true,
            const bool& match_stress = true);

    bool match(const Syllable& other) const;
};

struct Word
{
    std::string value;
    std::vector<Syllable> syllables;
};

struct Match
{
    std::vector<Word> sentence;
    std::vector<Syllable> pattern;
};

extern Syllable StressedSyllable;
extern Syllable UnstressedSyllable;
extern Syllable CodeSyllable;

#endif  // TYPES_HPP

