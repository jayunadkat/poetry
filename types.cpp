#include "types.hpp"

Syllable::Syllable(const std::string& code, const bool& stressed,
        const bool& match_code, const bool& match_stress) :
    code(code),
    stressed(stressed),
    match_code(match_code),
    match_stress(match_stress)
{
}

bool Syllable::match(const Syllable& other) const
{
    bool result(true);
    if (other.match_code)
        result = result && (code == other.code);
    if (other.match_stress)
        result = result && (stressed == other.stressed);
    return result;
}

Syllable StressedSyllable {"*", true, false, true};
Syllable UnstressedSyllable {"*", false, false, true};
Syllable CodeSyllable {"*", false, true, false};

