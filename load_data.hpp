#ifndef LOAD_DATA_HPP
#define LOAD_DATA_HPP

#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "types.hpp"

std::string read_file(const std::string& filename);
void load_line_data(const std::string& raw_line_data, std::string& word,
        std::vector<Syllable>& syllables);
std::vector<Word> read_dictionary(std::string raw_data);

#endif  // LOAD_DATA_HPP

