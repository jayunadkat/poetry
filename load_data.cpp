#include <cctype>

#include "load_data.hpp"

std::string read_file(const std::string& filename)
{
    std::ifstream datafile(filename);
    std::stringstream ss;
    ss << datafile.rdbuf();
    datafile.close();
    return ss.str();
}

void load_line_data(const std::string& raw_line_data, std::string& word,
        std::vector<Syllable>& syllables)
{
    syllables.clear();
    word = "";

    if (!std::isalpha(raw_line_data[0]))
        return;

    std::istringstream iss(raw_line_data);
    iss >> word;
    std::string datum;
    while (iss >> datum)
    {
        if (std::isdigit(datum.back()))
        {
            bool stressed = (datum.back() == '0' ? false : true);
            syllables.push_back({datum.substr(0, datum.size() - 1), stressed,
                    false, false});
        }
    }
}

std::vector<Word> read_dictionary(std::string raw_data)
{
    std::vector<Word> words;

    std::istringstream iss(raw_data);
    std::string raw_line_data, word;
    std::vector<Syllable> line_data;
    while (std::getline(iss, raw_line_data))
    {
        load_line_data(raw_line_data, word, line_data);
        if (word != "")
            words.push_back({word, line_data});
    }

    return words;
}

